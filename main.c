/****************************************************************

 main.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "../apesdk/toolkit/toolkit.h"
#include "element.h"
#include "pnglite.h"

#include <stdio.h>

element_document base_document;

extern n_object * mapout_input(n_file *file_json, element_document * document);
extern n_int mapout_render(element_document * document);
extern n_file * mapout_file(n_constant_string file_name);


static n_string_block json_file = "{\"canvas\":{\"size\":[1500,1280],\"file\":\"ashford.png\"},\"poly\":[{\"type\":\"green\",\"identifier\":1,\"points\":[[10,10],[15,10],[16,20],[18,25]]},{\"type\":\"contour\",\"identifier\":2,\"points\":[[210,210],[215,210],[216,220],[218,225]]},{\"type\":\"building\",\"identifier\":3,\"points\":[[110,110],[115,110],[116,120],[118,125],[120,130]]}],\"text\":[{\"string\":\"Mill House\",\"point\":[200,50]},{\"string\":\"Buxford House\",\"point\":[50,500]},{\"string\":\"Great Chart\",\"point\":[50,850],\"size\":2},{\"string\":\"Singleton\",\"point\":[150,910]},{\"string\":\"Brisley Farm\",\"point\":[200,1100]},{\"string\":\"Millbank Place\",\"point\":[400,1050]},{\"string\":\"Beaver\",\"point\":[600,950]},{\"string\":\"Beaver Green\",\"point\":[700,900]},{\"string\":\"Clock House\",\"point\":[150,850]},{\"string\":\"South Ashford School\",\"point\":[350,850]},{\"string\":\"Ashford\",\"size\":3,\"point\":[350,700]},{\"string\":\"Ford\",\"point\":[150,500]},{\"string\":\"Potters Corner\",\"point\":[150,250]},{\"string\":\"Golf Course\",\"point\":[150,400]},{\"string\":\"The Warren Hospital\",\"point\":[500,150]},{\"string\":\"Sanitorium\",\"point\":[700,50]},{\"string\":\"Bybrook\",\"point\":[700,300]},{\"string\":\"Sewage Works\",\"point\":[800,350]},{\"string\":\"Cemetery\",\"point\":[700,400]},{\"string\":\"Railway Works\",\"point\":[700,800]},{\"string\":\"South Willesborough Moat\",\"point\":[750,900]},{\"string\":\"Willesborough\",\"size\":2,\"point\":[850,800]},{\"string\":\"Court Lodge\",\"point\":[900,850]},{\"string\":\"Boys Hall\",\"point\":[900,900]}]}";

n_int draw_error( n_constant_string error_text, n_constant_string location, n_int line_number )
{
    printf( "ERROR: %s @ %s %ld\n", ( const n_string ) error_text, location, line_number );
    return -1;
}


int main(int argc, const char * argv[]) {
    n_file * output_file = io_file_new();

    n_object * current = mapout_input(mapout_file(argv[1]), &base_document);
    
//    n_object * current = mapout_input(io_file_new_from_string_block(json_file));
    
    mapout_render(&base_document);
    
    object_top_object(output_file, current);
    
    io_output_contents(output_file);
    obj_free( &current );
    io_file_free(&output_file);
    return 0;
}

