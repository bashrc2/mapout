/****************************************************************

 element.h

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "../../apesdk/toolkit/toolkit.h"

typedef enum
{
    EWT_ROAD = 1,
    EWT_RAIL,
    EWT_WATER,
    EWT_PATH,
    EWT_NO_WAY
} element_way_type;

typedef enum
{
    EPT_GREEN,
    EPT_CONTOUR,
    EPT_BUILDING,
    
    EPT_ROAD,
    EPT_RAIL,
    EPT_PATH,
    EPT_WATER,
    
    EPT_NO_TYPE
} element_poly_type;

typedef struct
{
    n_vect2 point;
    memory_list * way_ids;
}element_junction;

typedef struct
{
    n_vect2 point;
    n_int over_way_id;
    n_int under_way_id;
}element_bridge;

typedef struct
{
    element_way_type enum_type;
    n_int           way_id;
    memory_list     *points;   /* n_vect2 */
    memory_list     *junctions;/* element_junction */
    memory_list     *bridges;  /* element_bridge */
} element_way;

typedef struct
{
    n_vect2        point;
    n_int          size;
    n_string       string;
} element_text;

typedef struct
{
    element_poly_type enum_type;
    n_int             identifier;
    memory_list      *points;
} element_poly;

typedef struct{
    memory_list    * text;
    memory_list    * poly;
}element_document;


n_int element_unwrap_poly( n_string obj, n_byte * buffer );
n_int element_unwrap_text( n_string obj, n_byte * buffer );

n_object * element_poly_out(element_poly * input_poly);

n_object * element_text_out(element_text * input_text);

n_int element_document_in(n_object * input_json, element_document * output_document);
n_object * element_document_out(element_document * input_document);

void element_document_render(element_document * input_document);
n_int element_document_disk_read( n_string file_name );

