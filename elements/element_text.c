/****************************************************************

 element_text.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "element.h"

n_int element_text_in(n_object * input_json, element_text * output_text)
{
    n_vect2 temp;
    n_int   return_value = object_name_vect2("point", &temp, input_json);
    n_string string_element = obj_contains(input_json, "string", OBJECT_STRING);
    n_string size_element = obj_contains(input_json, "size", OBJECT_NUMBER);
    if ((return_value == 1) && string_element)
    {
        if (output_text)
        {
            output_text->string = io_string_copy(string_element);
            if (size_element)
            {
                output_text->size = obj_get_number(size_element);
            }
            else
            {
                output_text->size = 1;
            }
            output_text->point.x = temp.x;
            output_text->point.y = temp.y;
            return 1;
        }
    }
    return 0;
}

n_int element_unwrap_text( n_string obj, n_byte * buffer )
{
    return element_text_in( obj_get_object(obj), (element_text *)buffer);
}

n_object * element_text_out(element_text * input_text)
{
    n_object * output_json = 0L;
    if (input_text)
    {
        output_json = object_vect2_name(0L, "point", &(input_text->point));
        if (input_text->size != 1)
        {
            object_number(output_json, "size", input_text->size);
        }
        object_string(output_json, "string", input_text->string);
    }
    return output_json;
}
