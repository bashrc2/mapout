/****************************************************************

 element_way.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "element.h"

element_bridge * element_bridge_in(n_object * input_json)
{
    element_bridge * output_bridge = 0L;
    n_string str_point = obj_contains(input_json, "point", OBJECT_ARRAY);
    n_string str_over_way_id = obj_contains(input_json, "over_way_id", OBJECT_NUMBER);
    n_string str_under_way_id = obj_contains(input_json, "under_way_id", OBJECT_NUMBER);
    
    if (str_point && str_over_way_id && str_under_way_id)
    {
        n_array * point_array = obj_get_array(str_point);
        n_int number_elements = obj_array_count(point_array);
        if (number_elements == 2)
        {
            output_bridge = memory_new(sizeof(element_bridge));
            if (output_bridge)
            {
                output_bridge->over_way_id = obj_get_number(str_over_way_id);
                output_bridge->under_way_id = obj_get_number(str_under_way_id);
                (void)obj_contains_array_numbers(input_json, "point", (n_int *)&output_bridge->point, 2);
            }
        }
    }
    return output_bridge;
}

n_object * element_bridge_out(element_bridge * input_bridge)
{
    n_object * output_json = object_vect2_name(0L, "point", &input_bridge->point);    
    object_number(output_json, "over_way_id", input_bridge->over_way_id);
    object_number(output_json, "under_way_id", input_bridge->under_way_id);
    return output_json;
}

element_way * element_way_in(n_object * input_json)
{
    element_way * output_way = memory_new(sizeof(element_way));
    if (output_way)
    {
        
    }
    return output_way;
}


n_object * element_way_out(element_way * input_way)
{
    n_object * output_json = 0L;
    
    return output_json;
}
