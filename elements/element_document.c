/****************************************************************

 element_document.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include <stdio.h>
#include "../../apesdk/render/glrender.h"
#include "element.h"

n_int element_document_in(n_object * input_json, element_document * output_document)
{
    n_string text_element = obj_contains(input_json, "text", OBJECT_ARRAY);
    n_string poly_element = obj_contains(input_json, "poly", OBJECT_ARRAY);
 
    if (text_element)
    {
        output_document->text = object_unwrap_array(obj_get_array(text_element), sizeof(element_text),  element_unwrap_text, OBJECT_OBJECT);
    }
    if (poly_element)
    {
        output_document->poly = object_unwrap_array(obj_get_array(poly_element), sizeof(element_poly),  element_unwrap_poly, OBJECT_OBJECT);
    }
    if (text_element || poly_element)
    {
        return 1;
    }
    
    return SHOW_ERROR("document missing");
}

void element_document_render(element_document * input_document)
{
    glrender_reset();

    glrender_start_text_list();
    glrender_color(GLR_BLACK);
    {
        n_uint loop = 0;
        n_uint count = input_document->text->count;
        element_text * text_data = (element_text *) input_document->text->data;
        while (loop < count)
        {
            glrender_string(text_data[loop].string, text_data[loop].point.x, text_data[loop].point.y);
            loop++;
        }
        
    }
    glrender_end_text_list();
    
    glrender_start_display_list();
    
    {
        n_uint loop = 0;
        n_uint count = input_document->poly->count;
        element_poly * poly_data = (element_poly *) input_document->poly->data;
        while (loop < count)
        {
            element_poly_type ept = poly_data[loop].enum_type;
            n_vect2   *points = (n_vect2   *)poly_data[loop].points->data;
            n_uint    loop2 = 0;
            n_uint    points_count = poly_data[loop].points->count;
            switch (ept)
            {
                case EPT_GREEN:
                    glrender_color(GLR_GREEN);
                    break;
                    
                case EPT_CONTOUR:
                    glrender_color(GLR_ORANGE);
                    break;
                case EPT_BUILDING:
                    glrender_color(GLR_BLACK);
                    break;

                    
                case EPT_ROAD:
                    glrender_color(GLR_ORANGE);
                    break;
                    
                case EPT_RAIL:
                    glrender_color(GLR_BLACK);
                    break;

                case EPT_PATH:
                    glrender_color(GLR_GREY);
                    break;

                case EPT_WATER:
                    glrender_color(GLR_LIGHT_GREY);
                    break;
                default:
                    break;
            }
            
            while (loop2 < (points_count - 1))
            {
                printf("%ld, %ld\n", points[loop2].x, points[loop2].y);
                glrender_line(&points[loop2],&points[loop2 + 1]);
                loop2++;
            }
            
            switch (ept)
            {
                case EPT_GREEN:
                case EPT_CONTOUR:
                case EPT_BUILDING:
                    glrender_line(&points[0], &points[loop2 + 1]);
                    break;

                    
                case EPT_ROAD:
                case EPT_RAIL:
                case EPT_PATH:
                case EPT_WATER:
                default:
                    
                    break;
            }
            loop++;
        }
        
    }
    
    glrender_end_display_list();
}

n_object * element_document_out(element_document * input_document)
{
    n_object * output_json = 0L;
    if (input_document)
    {
        if (input_document->text)
        {
            element_text * text_entries = (element_text *)input_document->text->data;
            n_int loop = 0;
            n_int count = input_document->text->count;
            n_array  * text_array = 0L;
            while (loop < count)
            {
                n_object * text_element = element_text_out(&(text_entries[loop]));
                if (text_array)
                {
                    array_add(text_array, array_object(text_element));
                }
                else
                {
                    text_array = array_add(0L, array_object(text_element));
                }
                loop++;
            }
            
            if (output_json)
            {
                object_array(output_json, "text", text_array);
            }
            else
            {
                output_json = object_array(0L, "text", text_array);
            }
        }
        if (input_document->poly)
        {
            element_poly * poly_entries = (element_poly *)input_document->poly->data;
            n_int loop = 0;
            n_int count = input_document->poly->count;
            n_array  * poly_array = 0L;
            while (loop < count)
            {
                n_object * poly_element = element_poly_out(&(poly_entries[loop]));
                if (poly_array)
                {
                    array_add(poly_array, array_object(poly_element));
                }
                else
                {
                    poly_array = array_add(poly_array, array_object(poly_element));
                }
                loop++;
            }
            if (output_json)
            {
                object_array(output_json, "poly", poly_array);
            }
            else
            {
                output_json = object_array(0L, "poly", poly_array);
            }
        }
    }
    return output_json;
}



n_int element_document_disk_read( n_string file_name )
{
    n_file   *in_file = io_file_new();
    n_int    file_error = io_disk_read( in_file, file_name );

    io_whitespace_json( in_file );

//    printf( "%s --- \n", file_name );
    
    if ( file_error != -1 )
    {
        n_object_type type_of;
        void *returned_blob = unknown_file_to_tree( in_file, &type_of );
        io_file_free( &in_file );
        if ( returned_blob )
        {
            if (type_of == OBJECT_OBJECT)
            {
                {
                    element_document document;
                    n_int return_value = element_document_in((n_object *)returned_blob, &document);
                    if (return_value == 1)
                    {
                        n_object* return_object = element_document_out(&document);
                        if (return_object)
                        {
                            
                            element_document_render(&document);
                            
                            unknown_free( &returned_blob, type_of );
                            returned_blob = (void *)return_object;
                        }
                        else
                        {
                            return SHOW_ERROR( "no object created from document" );
                        }
                    }
                    else
                    {
                        return SHOW_ERROR( "failed document in" );
                    }
                    
                }
                {
                    n_file   *output_file = unknown_json( returned_blob, type_of );
                    if ( output_file )
                    {
                        n_string file_out = io_string_copy( file_name );
                        n_int    file_name_length = io_length( file_name, STRING_BLOCK_SIZE );
                        file_out[file_name_length - 6] = '2';
                        
                        file_error = io_disk_write( output_file, file_out );
                        io_file_free( &output_file );
                    }
                    else
                    {
                        return SHOW_ERROR( "no returned output file" );
                    }
                }
            }
            else
            {
                return SHOW_ERROR( "wrong json format" );
            }
            unknown_free( &returned_blob, type_of );
        }
        else
        {
            return SHOW_ERROR( "no returned object" );
        }
        return SHOW_SUCCESS;
    }

    return SHOW_ERROR( "reading from disk failed" );
}

