/****************************************************************

 element_poly.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "element.h"

n_int element_poly_in(n_object * input_json, element_poly * output_poly)
{
    if (output_poly)
    {
        n_string type_element = obj_contains(input_json, "type", OBJECT_STRING);
        n_string identifier_element = obj_contains(input_json, "identifier", OBJECT_NUMBER);
        n_string points_element = obj_contains(input_json, "points", OBJECT_ARRAY);

        n_uint     green_hash = math_hash( ( n_byte * )"green", 5 );
        n_uint     contour_hash = math_hash( ( n_byte * )"contour", 7 );
        n_uint     building_hash = math_hash( ( n_byte * )"building", 8);
        
        n_uint     road_hash = math_hash( ( n_byte * )"road", 4 );
        n_uint     rail_hash = math_hash( ( n_byte * )"rail", 4 );
        n_uint     path_hash = math_hash( ( n_byte * )"path", 4 );
        n_uint     water_hash = math_hash( ( n_byte * )"water", 5 );
        
        n_int      string_length = io_length( type_element, STRING_BLOCK_SIZE );
        n_uint     string_hash = math_hash( ( n_byte * )type_element, string_length);
        
        element_poly_type string_type = EPT_NO_TYPE;
        
        if (string_hash == green_hash) string_type = EPT_GREEN;
        if (string_hash == contour_hash) string_type = EPT_CONTOUR;
        if (string_hash == building_hash) string_type = EPT_BUILDING;

        if (string_hash == road_hash) string_type = EPT_ROAD;
        if (string_hash == rail_hash) string_type = EPT_RAIL;
        if (string_hash == path_hash) string_type = EPT_PATH;
        if (string_hash == water_hash) string_type = EPT_WATER;
        
        if ((string_type != EPT_NO_TYPE) && points_element)
        {
            if (output_poly)
            {
                output_poly->points = object_list_vect2(obj_get_array(points_element));
                output_poly->enum_type = string_type;
                if (identifier_element)
                {
                    output_poly->identifier = obj_get_number(identifier_element);
                }
                else
                {
                    output_poly->identifier = 0;
                }
            }
            return 1;
        }
    }
    return 0;
}

n_int element_unwrap_poly( n_string obj, n_byte * buffer )
{
    return element_poly_in( obj_get_object(obj), (element_poly *)buffer);
}

n_object * element_poly_out(element_poly * input_poly)
{
    n_object * output_json = 0L;
    if (input_poly)
    {
        output_json = object_vect2_names("points", input_poly->points);
        if (input_poly->identifier)
        {
            object_number(output_json, "identifier", input_poly->identifier);
        }
        if (input_poly->enum_type == EPT_GREEN)
        {
            object_string(output_json, "type", "green");
        }
        else if (input_poly->enum_type == EPT_CONTOUR)
        {
            object_string(output_json, "type", "contour");
        }
        else if (input_poly->enum_type == EPT_BUILDING)
        {
            object_string(output_json, "type", "building");
        }
        else if (input_poly->enum_type == EPT_ROAD)
        {
            object_string(output_json, "type", "road");
        }
        else if (input_poly->enum_type == EPT_RAIL)
        {
            object_string(output_json, "type", "rail");
        }
        else if (input_poly->enum_type == EPT_PATH)
        {
            object_string(output_json, "type", "path");
        }
        else if (input_poly->enum_type == EPT_WATER)
        {
            object_string(output_json, "type", "water");
        }
    }
    return output_json;
}
