#ifndef MAP2JSON_HEADERS_H
#define MAP2JSON_HEADERS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <toolkit.h>
#include "lodepng.h"

/* image dimensions in pixels */
#define MAX_TILE_WIDTH                 2000
#define MAX_TILE_HEIGHT                1300
#define MIN_SOURCE_MAP_IMAGE_WIDTH     10000
#define MIN_SOURCE_MAP_IMAGE_HEIGHT    10000

#define MAX_TOTAL_POLYGON_POINTS       (4096*8)
#define MAX_ROAD_POINTS                2048
#define MAX_JUNCTIONS                  2048
#define MAX_STATION_POINTS             32
#define MAX_BRIDGES                    64

/* Max number of points for an individual polygon, prior to reduction */
#define MAX_POLYGON_POINTS             4096

/* Background value for thresholded images */
#define BACKGROUND                     255

/* You may need to adjust this to avoid running out of stack space.
Stack size can be changed in /etc/security/limits.conf if needed  */
#define MAX_RECURSION_DEPTH            50000

/* This should not be a huge number. A few samples is enough to check
   that a line between points should probably exist */
#define LINE_EVIDENCE_SAMPLES          10

/* lodepng.c */

unsigned char * read_png_file(char * filename,
                              n_byte4 * width,
                              n_byte4 * height,
                              n_byte4 * bitsperpixel);

n_c_int write_png_file(char* filename,
                       n_byte4 width, n_byte4 height,
                       n_byte4 bitsperpixel,
                       unsigned char *buffer);

/* threshold.c */

n_c_int proximal_threshold(n_byte img[], n_c_int width, n_c_int height,
                           n_c_int r, n_c_int g, n_c_int b, n_c_int radius,
                           n_byte result[]);
n_c_int proximal_erase(n_byte img[], n_c_int width, n_c_int height,
                       n_c_int radius, n_c_int min_coverage_percent);
n_c_int proximal_fill(n_byte img[], n_c_int width, n_c_int height,
                      n_byte result[],
                      n_c_int min_width, n_c_int min_height,
                      n_c_int max_width, n_c_int max_height,
                      n_c_int max_variance,
                      n_c_int polygon_id[],
                      n_c_int polygon_vertices[],
                      n_c_int polygons[],
                      n_c_int max_total_polygon_points,
                      n_c_int detect_interior);
n_c_int show_polygons(n_byte img[], n_c_int width, n_c_int height,
                      n_c_int no_of_polygons,
                      n_c_int polygon_id[],
                      n_c_int polygon_vertices[],
                      n_c_int polygons[]);
n_c_int point_in_polygon(n_c_int x, n_c_int y, n_c_int * points, n_c_int no_of_points);
n_c_int remove_nogo_areas(n_byte img[], n_c_int width, n_c_int height,
                          n_c_int no_of_nogo_areas,
                          n_c_int vertices[], n_c_int nogo_areas[]);
n_c_int percent_coverage(n_byte img[], n_c_int width, n_c_int height);
n_c_int get_polygons_total_area(n_c_int width, n_c_int height,
                                n_c_int no_of_polygons,
                                n_c_int vertices[], n_c_int polygons[]);
n_c_int show_polygons_filled(n_byte img[], n_c_int width, n_c_int height,
                             n_c_int no_of_polygons,
                             n_c_int polygon_vertices[],
                             n_c_int polygons[],
                             n_c_int r, n_c_int g, n_c_int b);
n_c_int show_polygons_empty(n_byte img[], n_c_int width, n_c_int height,
                            n_c_int no_of_polygons,
                            n_c_int polygon_vertices[],
                            n_c_int polygon_id[],
                            n_c_int polygons[]);
n_c_int show_polygons_against_reference(n_byte img[], n_c_int width, n_c_int height,
                                        n_byte thresholded_ref[],
                                        n_c_int no_of_polygons,
                                        n_c_int polygon_id[],
                                        n_c_int polygon_vertices[],
                                        n_c_int polygons[],
                                        n_byte inner);

/* json.c */

n_c_int save_map_json(char source_filename[],
                      n_c_int no_of_polygons,
                      n_c_int * polygon_id,
                      n_c_int * polygon_vertices,
                      n_c_int * polygons,
                      n_c_int no_of_main_road_points,
                      n_c_int main_road_points[],
                      n_c_int main_road_links[],
                      n_c_int no_of_minor_road_points,
                      n_c_int minor_road_points[],
                      n_c_int minor_road_links[],
                      n_c_int no_of_woods,
                      n_c_int woods_id[],
                      n_c_int woods_vertices[],
                      n_c_int woods[],
                      n_c_int no_of_sea,
                      n_c_int sea_id[],
                      n_c_int sea_vertices[],
                      n_c_int sea[],
                      n_c_int no_of_water_points,
                      n_c_int water_points[],
                      n_c_int water_links[],
                      n_c_int no_of_stations,
                      n_c_int station_points[],
                      n_c_int sea_area_percent,
                      n_c_int sea_threshold,
                      n_c_int bridge_points[],
                      n_c_int no_of_bridges,
                      n_c_int junction_points[], n_c_int no_of_junctions,
                      n_c_int image_width, n_c_int image_height,
                      char * filename);

/* convert.c */

void mono_to_color(n_byte * img, n_c_int width, n_c_int height,
                   n_c_int bitsperpixel,
                   n_byte * color);

void color_to_mono(n_byte * img, n_c_int width, n_c_int height,
                   n_c_int bitsperpixel,
                   n_byte * mono);

void color_to_binary(n_byte * img, n_c_int width, n_c_int height,
                     n_c_int bitsperpixel, n_c_int threshold,
                     n_byte * mono);

/* color.c */

void detect_color(n_byte img[],
                  n_c_int width, n_c_int height, n_c_int bitsperpixel,
                  n_c_int primary_channel,
                  n_c_int red_low, n_c_int red_high,
                  n_c_int green_low, n_c_int green_high,
                  n_c_int blue_low, n_c_int blue_high,
                  n_c_int averaging_radius);
void detect_green(n_byte img[],
                  n_c_int width, n_c_int height, n_c_int bitsperpixel,
                  n_c_int threshold,
                  n_c_int averaging_radius);
void detect_orange(n_byte img[],
                   n_c_int width, n_c_int height, n_c_int bitsperpixel,
                   n_c_int threshold_red,
                   n_c_int threshold_green,
                   n_c_int averaging_radius,
                   n_c_int min_red, n_c_int max_red);
void detect_red(n_byte img[],
                n_c_int width, n_c_int height, n_c_int bitsperpixel,
                n_c_int threshold_red,
                n_c_int threshold_green,
                n_c_int averaging_radius,
                n_c_int min_red, n_c_int max_red,
                n_c_int max_green);
void detect_blue(n_byte img[],
                 n_c_int width, n_c_int height, n_c_int bitsperpixel,
                 n_c_int threshold_red,
                 n_c_int threshold_green,
                 n_c_int averaging_radius,
                 n_c_int min_blue, n_c_int max_blue);

/* random.c */

n_byte4 rand_num(n_byte4 * seed);

/* draw.c */
void draw_line(n_byte img[],
               n_byte4 width, n_byte4 height,
               n_c_int bitsperpixel,
               n_c_int tx, n_c_int ty, n_c_int bx, n_c_int by,
               n_c_int line_width,
               n_c_int r, n_c_int g, n_c_int b);

void draw_point(n_byte img[],
                n_byte4 width, n_byte4 height,
                n_c_int x, n_c_int y,
                n_c_int point_radius,
                n_c_int r, n_c_int g, n_c_int b);

/* skeleton.c */
void skeletonize(n_byte img[], n_c_int width, n_c_int height,
                 n_byte result[], n_c_int bitsperpixel,
                 n_c_int max_line_width);
n_c_int skeleton_to_points(n_byte img[], n_c_int width, n_c_int height,
                           n_c_int bitsperpixel, n_c_int point_spacing,
                           n_c_int line_points[], n_c_int max_points);

/* line.c */
n_c_int show_line_points(n_byte * result, n_c_int width, n_c_int height,
                         n_c_int line_points[], n_c_int no_of_line_points);

n_c_int link_line_points(n_c_int line_points[], n_c_int no_of_line_points,
                         n_c_int max_distance, n_c_int line_links[],
                         n_byte thresholded_ref[],
                         n_c_int image_width, n_c_int image_height);

n_c_int show_lines(n_byte result[], n_c_int width, n_c_int height, n_c_int bitsperpixel,
                   n_c_int line_points[], n_c_int no_of_line_points,
                   n_c_int line_links[], n_c_int line_width,
                   n_c_int r, n_c_int g, n_c_int b);
n_c_int remove_close_points(n_c_int line_points[],
                            n_c_int no_of_line_points,
                            n_c_int radius);
n_c_int show_three_lines(n_byte result[], n_c_int width, n_c_int height,
                         n_c_int bitsperpixel,
                         n_c_int line_points1[], n_c_int no_of_line_points1,
                         n_c_int line_links1[], n_c_int line_width1,
                         n_c_int r1, n_c_int g1, n_c_int b1,
                         n_c_int line_points2[], n_c_int no_of_line_points2,
                         n_c_int line_links2[], n_c_int line_width2,
                         n_c_int r2, n_c_int g2, n_c_int b2,
                         n_c_int line_points3[], n_c_int no_of_line_points3,
                         n_c_int line_links3[], n_c_int line_width3,
                         n_c_int r3, n_c_int g3, n_c_int b3,
                         n_c_int junction_points[], n_c_int no_of_junctions);
n_c_int detect_junctions(n_c_int line_points1[], n_c_int no_of_line_points1,
                         n_c_int line_links1[],
                         n_c_int line_points2[], n_c_int no_of_line_points2,
                         n_c_int line_links2[],
                         n_c_int junction_points[], n_c_int max_junctions,
                         n_c_int join_ends_radius);
n_c_int line_join_ends(n_c_int line_points[], n_c_int no_of_line_points,
                       n_c_int line_links[], n_c_int join_ends_radius);
n_c_int detect_crossings(n_c_int line_points1[], n_c_int no_of_line_points1,
                         n_c_int line_links1[],
                         n_c_int line_points2[], n_c_int no_of_line_points2,
                         n_c_int line_links2[],
                         n_c_int line_points3[], n_c_int no_of_line_points3,
                         n_c_int line_links3[],
                         n_c_int crossing_points[], n_c_int max_crossings);
n_c_int show_crossings(n_byte result[], n_c_int width, n_c_int height,
                       n_c_int bitsperpixel,
                       n_c_int crossing_points[], n_c_int no_of_crossings,
                       n_c_int r1, n_c_int g1, n_c_int b1,
                       n_c_int r2, n_c_int g2, n_c_int b2,
                       n_c_int crossing_size, n_c_int line_width);
n_c_int line_intersection(n_c_int line1_tx, n_c_int line1_ty,
                          n_c_int line1_bx, n_c_int line1_by,
                          n_c_int line2_tx, n_c_int line2_ty,
                          n_c_int line2_bx, n_c_int line2_by,
                          n_c_int * ix, n_c_int * iy);
n_c_int point_dist_from_line(n_c_int x0, n_c_int y0,
                             n_c_int x1, n_c_int y1,
                             n_c_int point_x, n_c_int point_y);
n_c_int potential_roads(n_byte img[], n_c_int width, n_c_int height,
			n_byte result[],
			n_c_int min_width, n_c_int max_width);
n_c_int filter_potential_roads(n_byte img[], n_c_int width, n_c_int height,
			       n_byte possible_roads[]);

/* tile.c */

n_c_int breakup_large_map(char * large_filename,
                          n_byte large_map[], n_c_int large_width, n_c_int large_height,
                          n_c_int image_bitsperpixel,
                          char * tiles_directory,
			  n_c_int overlap_percent);

/* blob.c */

n_c_int detect_blobs(n_byte img[], n_c_int width, n_c_int height,
                     n_c_int search_radius,
                     n_c_int max_blob_points,
                     n_c_int blob_points[],
                     n_c_int min_blob_size,
                     n_c_int max_blob_size,
                     n_byte result[]);
n_c_int remove_blobs_from_points(n_c_int blob_points[], n_c_int no_of_blobs,
                                 n_c_int line_points[], n_c_int no_of_line_points,
                                 n_c_int search_radius);

#endif
