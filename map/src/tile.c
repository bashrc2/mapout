#include "map2json.h"
#include "toolkit.h"

n_c_int get_large_map_metadata(char * filename,
                               n_c_int * tiles_across, n_c_int * tiles_down,
                               n_c_int * large_tx, n_c_int * large_ty,
                               n_c_int * large_bx, n_c_int * large_by,
                               float * tx_long, float * ty_lat,
                               float * bx_long, float * by_lat)
{
    n_c_int retval = 1;

    /* NOTE: west is positive
       the lat/long values here might not be accurate, and adjustments
       might be needed to get maps to line up */

    if (strstr(filename, "160")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 564;
        *large_ty = 961;
        *large_bx = 10482;
        *large_by = 12102;
        *tx_long = 0 + (37.1/60.0);
        *ty_lat = 51 + (54.95/60.0);
        *bx_long = 0 + (3.4/60.0);
        *by_lat = 51 + (30.1/60.0);
        retval = 0;
    }
    else if (strstr(filename, "161")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 491;
        *large_ty = 996;
        *large_bx = 10404;
        *large_by = 12138;
        *tx_long = (2.2/60.0);
        *ty_lat = 51 + (54.4/60.0);
        *bx_long = (-31.2/60.0);
        *by_lat = 51 + (29.45/60.0);
        retval = 0;
    }
    else if (strstr(filename, "162")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 506;
        *large_ty = 946;
        *large_bx = 10442;
        *large_by = 12098;
        *tx_long = (-28.3/60.0);
        *ty_lat = 51 + (53.9/60.0);
        *bx_long = -1 - (1.4/60.0);
        *by_lat = 51 + (28.7/60.0);
        retval = 0;
    }
    else if (strstr(filename, "170")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 654;
        *large_ty = 956;
        *large_bx = 10572;
        *large_by = 12096;
        *tx_long = (37.9/60.0);
        *ty_lat = 51 + (30.6/60.0);
        *bx_long = (4.3/60.0);
        *by_lat = 51 + (5.8/60.0);
        retval = 0;
    }
    else if (strstr(filename, "171")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 542;
        *large_ty = 938;
        *large_bx = 10462;
        *large_by = 12106;
        *tx_long = (3.3/60.0);
        *ty_lat = 51 + (30.2/60.0);
        *bx_long = (-30/60.0);
        *by_lat = 51 + (5.2/60.0);
        retval = 0;
    }
    else if (strstr(filename, "172")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 469;
        *large_ty = 949;
        *large_bx = 10371;
        *large_by = 12135;
        *tx_long = (-27/60.0);
        *ty_lat = 51 + (29.6/60.0);
        *bx_long = (-59.8/60.0);
        *by_lat = 51 + (5.5/60.0);
        retval = 0;
    }
    else if (strstr(filename, "173")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 644;
        *large_ty = 963;
        *large_bx = 10657;
        *large_by = 12048;
        *tx_long = (-54.5/60.0);
        *ty_lat = 51 + (25.2/60.0);
        *bx_long = -1 - (27/60.0);
        *by_lat = 51;
        retval = 0;
    }
    else if (strstr(filename, "182")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 614;
        *large_ty = 1036;
        *large_bx = 10507;
        *large_by = 12235;
        *tx_long = 51 + (11.8/60.0);
        *ty_lat = (38.4/60.0);
        *bx_long = (5/60.0);
        *by_lat = 50 + (47/60.0);
        retval = 0;
    }
    else if (strstr(filename, "183")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 544;
        *large_ty = 1090;
        *large_bx = 10464;
        *large_by = 12268;
        *tx_long = (4.2/60.0);
        *ty_lat = 51 + (7.5/60.0);
        *bx_long = (-28.8/60.0);
        *by_lat = 50 + (42.5/60.0);
        retval = 0;
    }
    else if (strstr(filename, "184")) {
        *tiles_across = 7;
        *tiles_down = 11;
        *large_tx = 526;
        *large_ty = 955;
        *large_bx = 10456;
        *large_by = 12101;
        *tx_long = (-26.1/60.0);
        *ty_lat = 51 + (13.5/60.0);
        *bx_long = (-58.9/60.0);
        *by_lat = 50 + (48.3/60.0);
        retval = 0;
    }
    return retval;
}


/* copy an individual tile from a larger map image */
void copy_tile(n_byte large_map[], n_c_int large_width, n_byte tile_data[],
               n_c_int tx, n_c_int ty, n_c_int bx, n_c_int by)
{
    n_c_int x, y, n, n_tile, ch;
    n_c_int tile_width = bx - tx;

    for (y = ty; y < by; y++) {
        for (x = tx; x < bx; x++) {
            n = (y*large_width + x)*3;
            n_tile = ((y - ty)*tile_width + (x - tx))*3;
            for (ch=0; ch < 3; ch++) {
                tile_data[n_tile+ch] = large_map[n+ch];
            }
        }
    }
}

/* breaks up a large map image into smaller tiles and saves them
   to a tiles directory */
n_c_int breakup_large_map(char * large_filename,
                          n_byte large_map[], n_c_int large_width, n_c_int large_height,
                          n_c_int image_bitsperpixel,
                          char * tiles_directory,
			  n_c_int overlap_percent)
{
    n_c_int tile_x, tile_y, tx, ty, bx, by, tile_width, tile_height;
    n_byte * tile_data;
    n_c_int bytesperpixel = (image_bitsperpixel/8);
    n_c_int start_latitude, start_longitude;
    n_c_int end_latitude, end_longitude;
    char tile_filename[256];
    n_c_int tiles_across = 12;
    n_c_int tiles_down = 22;
    n_c_int large_tx = 0;
    n_c_int large_ty = 0;
    n_c_int large_bx = large_width;
    n_c_int large_by = large_height;
    float tx_long = 1;
    float ty_lat = 3;
    float bx_long = 4;
    float by_lat = 7;
    float fraction;

    if (get_large_map_metadata(large_filename,
                               &tiles_across, &tiles_down,
                               &large_tx, &large_ty,
                               &large_bx, &large_by,
                               &tx_long, &ty_lat,
                               &bx_long, &by_lat) != 0) {
        printf("No metadata for map %s\n", large_filename);
        return 2;
    }

    tile_width = (large_bx - large_tx) / tiles_across;
    tile_height = (large_by - large_ty) / tiles_down;

    /* expand the tile size for overlap */
    tile_width = tile_width * (100 + overlap_percent) / 100;
    tile_height = tile_height * (100 + overlap_percent) / 100;

    tile_data = (n_byte*)malloc(tile_width*tile_height*bytesperpixel*sizeof(n_byte));
    if (tile_data == NULL) {
        printf("Unable to allocate memory for tile.\n");
        return 1;
    }

    for (tile_y = 0; tile_y < tiles_down; tile_y++) {
        /* pixel vertical coords */
        ty = large_ty + (tile_y * (large_by - large_ty) / tiles_down);
        by = ty + tile_height;
	if (by > large_by) {
	  by = large_by;
	  ty = large_by - tile_height;
	}

	/* vertical geocoords */
	fraction = (ty - large_ty) / (float)(large_by - large_ty);
	start_latitude = (n_c_int)((ty_lat + (fraction * (by_lat - ty_lat))) * 1000);
	fraction = (by - large_ty) / (float)(large_by - large_ty);
	end_latitude = (n_c_int)((ty_lat + (fraction * (by_lat - ty_lat))) * 1000);

        for (tile_x = 0; tile_x < tiles_across; tile_x++) {
            /* pixel horizontal coords */
            tx = large_tx + (tile_x * (large_bx - large_tx) / tiles_across);
            bx = tx + tile_width;
	    if (bx > large_bx) {
	      bx = large_bx;
	      tx = large_bx - tile_width;
	    }

	    /* horizontal geocoords */
	    fraction = (tx - large_tx) / (float)(large_bx - large_tx);
	    start_longitude = (n_c_int)((tx_long + (fraction * (bx_long - tx_long))) * 1000);
	    fraction = (bx - large_tx) / (float)(large_bx - large_tx);
	    end_longitude = (n_c_int)((tx_long + (fraction * (bx_long - tx_long))) * 1000);

            sprintf(tile_filename,"%s/%d_%d-%d_%d-%d_%d-%d_%d.png", tiles_directory,
		    tile_y, tile_x, start_latitude, start_longitude,
		    end_latitude, end_longitude,
                    tx, ty);
            printf("Tile: %s\n", tile_filename);

            copy_tile(large_map, large_width, tile_data, tx, ty, bx, by);
            write_png_file(tile_filename, tile_width, tile_height, 24, tile_data);
        }
    }

    free(tile_data);
    return 0;
}
