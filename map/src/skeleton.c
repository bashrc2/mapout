#include "map2json.h"
#include "toolkit.h"

static void compact_skeleton(n_byte img[], n_c_int width, n_c_int height,
                             n_c_int bytesperpixel,
                             n_c_int compact_radius)
{
    n_c_int x, y, n, n2, ch, mid_pos, last_pos;

    /* left to right */
    for (y = 0; y < height; y++) {
        last_pos = -1;
        for (x = 0; x < width; x++) {
            n = (y*width + x)*3;
            if (img[n] == 0) {
                if (last_pos != -1) {
                    if (x - last_pos < compact_radius) {
                        /* clear the previous points */
                        n2 = (y*width + last_pos)*3;
                        for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                            img[n2] = BACKGROUND;
                        }
                        for (ch = 0; ch < bytesperpixel; ch++, n++) {
                            img[n] = BACKGROUND;
                        }
                        /* fill the intermedia position */
                        mid_pos = last_pos + ((x - last_pos)/2);
                        n2 = (y*width + mid_pos)*3;
                        for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                            img[n2] = 0;
                        }
                    }
                }
                last_pos = x;
            }
        }
    }

    /* top to bottom */
    for (x = 0; x < width; x++) {
        last_pos = -1;
        for (y = 0; y < height; y++) {
            n = (y*width + x)*3;
            if (img[n] == 0) {
                if (last_pos != -1) {
                    if (y - last_pos < compact_radius) {
                        /* clear the previous points */
                        n2 = (last_pos*width + x)*3;
                        for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                            img[n2] = BACKGROUND;
                        }
                        for (ch = 0; ch < bytesperpixel; ch++, n++) {
                            img[n] = BACKGROUND;
                        }
                        /* fill the intermedia position */
                        mid_pos = last_pos + ((y - last_pos)/2);
                        n2 = (mid_pos*width + x)*3;
                        for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                            img[n2] = 0;
                        }
                    }
                }
                last_pos = y;
            }
        }
    }
}

void skeletonize(n_byte img[], n_c_int width, n_c_int height,
                 n_byte result[], n_c_int bitsperpixel,
                 n_c_int max_line_width)
{
    n_c_int x, y, state, n, n2, ch;
    n_c_int start_pos, end_pos, mid_pos;
    n_c_int bytesperpixel = bitsperpixel/8;

    memset(result, BACKGROUND, width*height*bytesperpixel*sizeof(n_byte));

    /* top down */
    for (x = 0; x < width; x++) {
        state = 0;
        for (y = 0; y < height; y++) {
            n = y*width + x;
            if (state == 0) {
                if (img[n] == 0) {
                    state = 1;
                    start_pos = y;
                }
            }
            else {
                if (img[n] != 0) {
                    state = 0;
                    end_pos = y;
                    mid_pos = start_pos + ((end_pos - start_pos)/2);
                    n2 = (mid_pos*width + x)*bytesperpixel;
                    for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                        result[n2] = 0;
                    }
                }
                else {
                    if (y - start_pos > max_line_width) {
                        state = 0;
                    }
                }
            }
        }
    }

    /* bottom up */
    for (x = width-1; x >= 0; x--) {
        state = 0;
        for (y = height-1; y >= 0; y--) {
            n = y*width + x;
            if (state == 0) {
                if (img[n] == 0) {
                    state = 1;
                    start_pos = y;
                }
            }
            else {
                if (img[n] != 0) {
                    state = 0;
                    end_pos = y;
                    mid_pos = start_pos + ((end_pos - start_pos)/2);
                    n2 = (mid_pos*width + x)*bytesperpixel;
                    for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                        result[n2] = 0;
                    }
                }
                else {
                    if (start_pos - y > max_line_width) {
                        state = 0;
                    }
                }
            }
        }
    }

    /* right to left */
    for (y = 0; y < height; y++) {
        state = 0;
        for (x = width - 1; x >= 0; x--) {
            n = y*width + x;
            if (state == 0) {
                if (img[n] == 0) {
                    state = 1;
                    start_pos = x;
                }
            }
            else {
                if (img[n] != 0) {
                    state = 0;
                    end_pos = x;
                    mid_pos = start_pos + ((end_pos - start_pos)/2);
                    n2 = (y*width + mid_pos)*bytesperpixel;
                    for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                        result[n2] = 0;
                    }
                }
                else {
                    if (start_pos - x > max_line_width) {
                        state = 0;
                    }
                }
            }
        }
    }

    /* left to right */
    for (y = 0; y < height; y++) {
        state = 0;
        for (x = 0; x < width; x++) {
            n = y*width + x;
            if (state == 0) {
                if (img[n] == 0) {
                    state = 1;
                    start_pos = x;
                }
            }
            else {
                if (img[n] != 0) {
                    state = 0;
                    end_pos = x;
                    mid_pos = start_pos + ((end_pos - start_pos)/2);
                    n2 = (y*width + mid_pos)*bytesperpixel;
                    for (ch = 0; ch < bytesperpixel; ch++, n2++) {
                        result[n2] = 0;
                    }
                }
                else {
                    if (x - start_pos > max_line_width) {
                        state = 0;
                    }
                }
            }
        }
    }
    compact_skeleton(result, width, height, bytesperpixel,
                     max_line_width/2);
}

n_c_int skeleton_to_points(n_byte img[], n_c_int width, n_c_int height,
                           n_c_int bitsperpixel, n_c_int point_spacing,
                           n_c_int line_points[], n_c_int max_points)
{
    n_c_int x, y, xx, yy, n, av_x, av_y, hits;
    n_c_int no_of_line_points = 0;

    for (y = 0; y < height; y+=point_spacing) {
        for (x = 0; x < width; x+=point_spacing) {
            hits = 0;
            av_x = 0;
            av_y = 0;
            for (yy = y; yy < y + point_spacing; yy++) {
                if (yy >= height) continue;
                for (xx = x; xx < x + point_spacing; xx++) {
                    if (xx >= width) continue;
                    n = (yy*width + xx)*3;
                    if (img[n] == BACKGROUND) continue;
                    av_x += xx;
                    av_y += yy;
                    hits++;
                }
            }
            if (hits == 0) continue;
            line_points[no_of_line_points*2] = av_x / hits;
            line_points[no_of_line_points*2+1] = av_y / hits;
            no_of_line_points++;
            if (no_of_line_points >= max_points) {
                return no_of_line_points;
            }
        }
    }
    return no_of_line_points;
}
