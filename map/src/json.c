#include "map2json.h"
#include "toolkit.h"

static void get_geocoords_from_filename(char filename[],
					n_c_int * tile_x, n_c_int * tile_y,
                                        float * long_tx, float * lat_ty,
                                        float * long_bx, float * lat_by,
                                        n_c_int * pixels_tx,
                                        n_c_int * pixels_ty)
{
    n_c_int index = strlen(filename)-1, buffer_index = 0, num_index = 0, found = 0;
    n_c_int coord_int = 0;
    char buffer[256];
    float coord;

    while (index > 0) {
        if (filename[index] == '/') {
            found = 1;
            break;
        }
        index--;
    }
    if (found == 1) {
        index++;
    }
    else {
        index = 0;
    }
    while(index < strlen(filename)) {
        if ((filename[index] == '_') ||
                (filename[index] == '-') ||
                (filename[index] == '.')) {
            buffer[buffer_index] = 0;
            if ((num_index >= 2) && (num_index < 6)) {
                coord = atof(buffer) / 1000.0;
            }
            else {
                coord_int = (n_c_int)atoi(buffer);
            }
            switch(num_index) {
            case 0: {
                *tile_y = coord_int;
                break;
            }
            case 1: {
                *tile_x = coord_int;
                break;
            }
            case 2: {
                *lat_ty = coord;
                break;
            }
            case 3: {
                *long_tx = coord;
                break;
            }
            case 4: {
                *lat_by = coord;
                break;
            }
            case 5: {
                *long_bx = coord;
                break;
            }
            case 6: {
                *pixels_tx = coord_int;
                break;
            }
            case 7: {
                *pixels_ty = coord_int;
                break;
            }
            }
            buffer_index = 0;
            num_index++;
            if (num_index == 8) break;
        }
        else {
            buffer[buffer_index++] = filename[index];
        }
        index++;
    }
}

n_c_int save_map_json(char source_filename[],
                      n_c_int no_of_polygons,
                      n_c_int * polygon_id,
                      n_c_int * polygon_vertices,
                      n_c_int * polygons,
                      n_c_int no_of_main_road_points,
                      n_c_int main_road_points[],
                      n_c_int main_road_links[],
                      n_c_int no_of_minor_road_points,
                      n_c_int minor_road_points[],
                      n_c_int minor_road_links[],
                      n_c_int no_of_woods,
                      n_c_int woods_id[],
                      n_c_int woods_vertices[],
                      n_c_int woods[],
                      n_c_int no_of_sea,
                      n_c_int sea_id[],
                      n_c_int sea_vertices[],
                      n_c_int sea[],
                      n_c_int no_of_water_points,
                      n_c_int water_points[],
                      n_c_int water_links[],
                      n_c_int no_of_stations,
                      n_c_int station_points[],
                      n_c_int sea_area_percent,
                      n_c_int sea_threshold,
                      n_c_int bridge_points[],
                      n_c_int no_of_bridges,
                      n_c_int junction_points[], n_c_int no_of_junctions,
                      n_c_int image_width, n_c_int image_height,
                      char * filename)
{
    n_c_int p, v;
    n_c_int vertex_index = 0;
    n_c_int woods_index = 0;
    n_c_int sea_index = 0;
    n_c_int is_interior = 0;
    n_c_int next_interior = 0;
    n_c_int pixels_tx = 0;
    n_c_int pixels_ty = 0;
    n_c_int tile_x = 0, tile_y = 0;
    FILE * fp;
    float long_tx = 0;
    float lat_ty = 0;
    float long_bx = 0;
    float lat_by = 0;

    get_geocoords_from_filename(source_filename,
				&tile_x, &tile_y,
                                &long_tx, &lat_ty, &long_bx, &lat_by,
                                &pixels_tx, &pixels_ty);

    fp = fopen(filename, "w");
    if (fp == 0) return 1;

    fprintf(fp, "{\n");
    fprintf(fp, "  \"sourceFilename\": \"%s\",\n", source_filename);
    fprintf(fp, "  \"tile\": [%d,%d],\n", tile_x, tile_y);
    fprintf(fp, "  \"offset\": [%d,%d],\n", pixels_tx, pixels_ty);
    fprintf(fp, "  \"resolution\": [%d,%d],\n", image_width, image_height);
    fprintf(fp, "  \"geocoords\": {\n");
    fprintf(fp, "    \"topleft\": {\n");
    fprintf(fp, "      \"latitude\": %f,\n", lat_ty);
    fprintf(fp, "      \"longitude\": %f\n", long_tx);
    fprintf(fp, "    },\n");
    fprintf(fp, "    \"bottomright\": {\n");
    fprintf(fp, "      \"latitude\": %f,\n", lat_by);
    fprintf(fp, "      \"longitude\": %f\n", long_bx);
    fprintf(fp, "    }\n");
    fprintf(fp, "  },\n");
    fprintf(fp, "  \"terrain\": [\n");

    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"woodland\",\n");
    fprintf(fp, "    \"polygons\": {");
    for (p = 0; p < no_of_woods; p++) {
        fprintf(fp, "    \"%d\": {\n", woods_id[p]);
        fprintf(fp, "      \"perimeter\": [");
        for (v = 0; v < woods_vertices[p]; v++, woods_index++) {
            if (v > 0) {
                fprintf(fp, ",[%d,%d]",
                        woods[woods_index*2]+pixels_tx,
                        woods[woods_index*2+1]+pixels_ty);
            }
            else {
                fprintf(fp, "[%d,%d]",
                        woods[woods_index*2]+pixels_tx,
                        woods[woods_index*2+1]+pixels_ty);
            }
        }
        fprintf(fp, "]\n");

        if (p < no_of_woods - 1) {
            fprintf(fp, "    },\n");
        }
        else {
            fprintf(fp, "    }\n");
        }
    }
    fprintf(fp, "  }\n");
    fprintf(fp, "  },\n");

    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"rivers\",\n");
    fprintf(fp, "    \"points\": [");
    for (p = 0; p < no_of_water_points; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    water_points[p*2]+pixels_tx, water_points[p*2+1]+pixels_ty);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    water_points[p*2]+pixels_tx, water_points[p*2+1]+pixels_ty);
        }
    }
    fprintf(fp, "],\n");
    fprintf(fp, "    \"links\": [");
    for (p = 0; p < no_of_water_points; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    water_links[p*2]-1, water_links[p*2+1]-1);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    water_links[p*2]-1, water_links[p*2+1]-1);
        }
    }
    fprintf(fp, "]\n");
    fprintf(fp, "  },\n");

    if (sea_area_percent > sea_threshold) {
        fprintf(fp, "  {\n");
        fprintf(fp, "    \"type\": \"lakes\",\n");
        fprintf(fp, "    \"polygons\": {}");
        fprintf(fp, "  },");
        fprintf(fp, "  {\n");
        fprintf(fp, "    \"type\": \"sea\",\n");
    }
    else {
        fprintf(fp, "  {\n");
        fprintf(fp, "    \"type\": \"sea\",\n");
        fprintf(fp, "    \"polygons\": {}");
        fprintf(fp, "  },");
        fprintf(fp, "  {\n");
        fprintf(fp, "    \"type\": \"lakes\",\n");
    }
    fprintf(fp, "    \"polygons\": {\n");
    for (p = 0; p < no_of_sea; p++) {
        fprintf(fp, "    \"%d\": {\n", sea_id[p]);
        fprintf(fp, "      \"perimeter\": [");
        for (v = 0; v < sea_vertices[p]; v++, sea_index++) {
            if (v > 0) {
                fprintf(fp, ",[%d,%d]",
                        sea[sea_index*2]+pixels_tx,
                        sea[sea_index*2+1]+pixels_ty);
            }
            else {
                fprintf(fp, "[%d,%d]",
                        sea[sea_index*2]+pixels_tx,
                        sea[sea_index*2+1]+pixels_ty);
            }
        }
        fprintf(fp, "]\n");

        if (p < no_of_sea - 1) {
            fprintf(fp, "    },\n");
        }
        else {
            fprintf(fp, "    }\n");
        }
    }
    fprintf(fp, "  }\n");
    fprintf(fp, "  }\n");
    fprintf(fp, "  ],\n");

    fprintf(fp, "  \"railways\": [\n");

    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"stations\",\n");
    fprintf(fp, "    \"points\": [");
    for (p = 0; p < no_of_stations; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    station_points[p*2]+pixels_tx, station_points[p*2+1]+pixels_ty);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    station_points[p*2]+pixels_tx, station_points[p*2+1]+pixels_ty);
        }
    }
    fprintf(fp, "]\n");
    fprintf(fp, "  }\n");
    fprintf(fp, "  ],\n");

    fprintf(fp, "  \"urban\": [\n");

    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"bridges\",\n");
    fprintf(fp, "    \"points\": [");
    for (p = 0; p < no_of_bridges; p++) {
        if (p > 0) {
            fprintf(fp, ",[[%d,%d],[%d,%d]]",
                    bridge_points[p*4]+pixels_tx, bridge_points[p*4+1]+pixels_ty,
                    bridge_points[p*4+2], bridge_points[p*4+3]);
        }
        else {
            fprintf(fp, "[[%d,%d],[%d,%d]]",
                    bridge_points[p*4]+pixels_tx, bridge_points[p*4+1]+pixels_ty,
                    bridge_points[p*4+2], bridge_points[p*4+3]);
        }
    }
    fprintf(fp, "]\n");
    fprintf(fp, "  },\n");
    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"junctions\",\n");
    fprintf(fp, "    \"points\": [");
    for (p = 0; p < no_of_junctions; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    junction_points[p*2]+pixels_tx, junction_points[p*2+1]+pixels_ty);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    junction_points[p*2]+pixels_tx, junction_points[p*2+1]+pixels_ty);
        }
    }
    fprintf(fp, "]\n");
    fprintf(fp, "  },\n");
    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"main roads\",\n");
    fprintf(fp, "    \"points\": [");
    for (p = 0; p < no_of_main_road_points; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    main_road_points[p*2]+pixels_tx, main_road_points[p*2+1]+pixels_ty);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    main_road_points[p*2]+pixels_tx, main_road_points[p*2+1]+pixels_ty);
        }
    }
    fprintf(fp, "],\n");
    fprintf(fp, "    \"links\": [");
    for (p = 0; p < no_of_main_road_points; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    main_road_links[p*2]-1, main_road_links[p*2+1]-1);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    main_road_links[p*2]-1, main_road_links[p*2+1]-1);
        }
    }
    fprintf(fp, "]\n");
    fprintf(fp, "  },\n");

    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"minor roads\",\n");
    fprintf(fp, "    \"points\": [");
    for (p = 0; p < no_of_minor_road_points; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    minor_road_points[p*2]+pixels_tx, minor_road_points[p*2+1]+pixels_ty);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    minor_road_points[p*2]+pixels_tx, minor_road_points[p*2+1]+pixels_ty);
        }
    }
    fprintf(fp, "],\n");
    fprintf(fp, "    \"links\": [");
    for (p = 0; p < no_of_main_road_points; p++) {
        if (p > 0) {
            fprintf(fp, ",[%d,%d]",
                    minor_road_links[p*2]-1, minor_road_links[p*2+1]-1);
        }
        else {
            fprintf(fp, "[%d,%d]",
                    minor_road_links[p*2]-1, minor_road_links[p*2+1]-1);
        }
    }
    fprintf(fp, "]\n");
    fprintf(fp, "  },\n");

    fprintf(fp, "  {\n");
    fprintf(fp, "    \"type\": \"buildings\",\n");
    fprintf(fp, "    \"polygons\": {\n");

    for (p = 0; p < no_of_polygons; p++) {
        is_interior = 0;
        if (p > 0) {
            if (polygon_id[p - 1] == polygon_id[p]) {
                is_interior = 1;
            }
        }
        if (is_interior == 0) {
            fprintf(fp, "    \"%d\": {\n", polygon_id[p]);
            fprintf(fp, "      \"ext\": [");
        }
        else {
            fprintf(fp, ",\n");
            fprintf(fp, "      \"int\": [");
        }
        for (v = 0; v < polygon_vertices[p]; v++, vertex_index++) {
            if (v > 0) {
                fprintf(fp, ",[%d,%d]",
                        polygons[vertex_index*2]+pixels_tx,
                        polygons[vertex_index*2+1]+pixels_ty);
            }
            else {
                fprintf(fp, "[%d,%d]",
                        polygons[vertex_index*2]+pixels_tx,
                        polygons[vertex_index*2+1]+pixels_ty);
            }
        }
        fprintf(fp, "]");
        next_interior = 0;
        if (p < no_of_polygons - 1) {
            if (polygon_id[p + 1] == polygon_id[p]) {
                next_interior = 1;
            }
        }
        if (next_interior == 0) {
            /* has no interior */
            fprintf(fp, "\n");
            if (p < no_of_polygons - 1) {
                fprintf(fp, "    },\n");
            }
            else {
                fprintf(fp, "    }\n");
            }
        }
    }

    /* end of buildings */
    fprintf(fp, "  }\n");
    fprintf(fp, "  }\n");
    fprintf(fp, "  ]\n");
    fprintf(fp, "}\n");

    fclose(fp);

    return 0;
}
