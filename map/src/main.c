#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "map2json.h"
#include "toolkit.h"

int main(int argc, char* argv[])
{
    n_c_int i;
    char * filename = NULL;
    char output_filename[255];
    n_byte * image_data = NULL;
    n_byte4 image_width=0;
    n_byte4 image_height=0;
    n_byte4 image_bitsperpixel=0;
    n_byte * thresholded = NULL;
    n_byte * thresholded_ref = NULL;
    n_byte * possible_roads = NULL;
    n_byte * original_data = NULL;
    n_c_int proximal_color[3];
    n_c_int proximal_radius=70;
    n_c_int proximal_coverage=23;
    n_c_int adjusted_coverage;
    n_c_int road_coverage = 12;
    n_c_int line_search_radius = 10;
    n_c_int max_building_size = 15;
    n_c_int max_building_polygon_width = 1700*max_building_size/100;
    n_c_int max_building_polygon_height = 1000*max_building_size/100;
    n_c_int current_coverage = 0;

    n_c_int polygon_vertices[MAX_TOTAL_POLYGON_POINTS];
    n_c_int polygon_id[MAX_TOTAL_POLYGON_POINTS];
    n_c_int polygons[MAX_TOTAL_POLYGON_POINTS*2];
    n_c_int no_of_polygons;

    n_c_int woods_vertices[MAX_TOTAL_POLYGON_POINTS];
    n_c_int woods_id[MAX_TOTAL_POLYGON_POINTS];
    n_c_int woods[MAX_TOTAL_POLYGON_POINTS*2];
    n_c_int no_of_woods;
    n_c_int woods_coverage = 25;
    n_c_int woods_averaging_radius = 1;
    n_c_int woods_threshold = 30;

    n_c_int sea_threshold_red_low = 160;
    n_c_int sea_threshold_red_high = 245;
    n_c_int sea_threshold_green_low = 180;
    n_c_int sea_threshold_green_high = 240;
    n_c_int sea_threshold_blue_low = 155;
    n_c_int sea_threshold_blue_high = 210;
    n_c_int min_sea_polygon_width = 1700/30;
    n_c_int min_sea_polygon_height = 1000/30;
    n_c_int sea_vertices[MAX_TOTAL_POLYGON_POINTS];
    n_c_int sea_id[MAX_TOTAL_POLYGON_POINTS];
    n_c_int sea[MAX_TOTAL_POLYGON_POINTS*2];
    n_c_int no_of_sea;
    n_c_int sea_coverage = 25;
    n_c_int sea_averaging_radius = 2;
    n_c_int sea_area_percent;
    char water_area_name[16];
    char water_filename[32];
    /* percentage of the image after which a water area is considered to be the sea */
    n_c_int sea_threshold = 10;

    n_c_int water_coverage = 5;
    n_c_int water_points[MAX_ROAD_POINTS*2];
    n_c_int no_of_water_points = 0;
    n_c_int water_links[MAX_ROAD_POINTS*2];
    n_c_int water_line_link_radius = 30;
    n_c_int max_river_width = 50;
    n_c_int water_threshold_red = 5;
    n_c_int water_threshold_green = 5;
    n_c_int water_averaging_radius = 1;
    n_c_int water_min_blue = 100;
    n_c_int water_max_blue = 200;
    n_c_int river_join_ends_radius = 40;

    n_c_int min_possible_road_width = 1;
    n_c_int max_possible_road_width = 20;
    n_c_int main_road_points[MAX_ROAD_POINTS*2];
    n_c_int no_of_main_road_points = 0;
    n_c_int no_of_links;
    n_c_int main_road_links[MAX_ROAD_POINTS*2];
    n_c_int road_line_link_radius = 50;
    n_c_int max_road_width = 50;
    n_c_int road_point_spacing = 20;
    n_c_int road_main_threshold_red = 70;
    n_c_int road_main_threshold_green = 20;
    n_c_int road_main_averaging_radius = 1;
    n_c_int road_main_min_red = 130;
    n_c_int road_main_max_red = 252;
    n_c_int road_main_max_green = 150;

    n_c_int minor_road_points[MAX_ROAD_POINTS*2];
    n_c_int no_of_minor_road_points = 0;
    n_c_int road_minor_threshold_red = 50;
    n_c_int road_minor_threshold_green = 20;
    n_c_int road_minor_min_red = 160;
    n_c_int road_minor_max_red = 241;
    n_c_int road_minor_averaging_radius = 1;
    n_c_int minor_road_join_ends_radius = 20;
    n_c_int minor_road_links[MAX_ROAD_POINTS*2];

    n_c_int no_of_junctions;
    n_c_int junction_points[MAX_JUNCTIONS*2];

    n_c_int station_points[MAX_STATION_POINTS*2];
    n_c_int min_station_size = 5;
    n_c_int max_station_size = 20;
    n_c_int no_of_stations = 0;

    n_c_int bridge_points[MAX_BRIDGES*4];
    n_c_int no_of_bridges = 0;

    n_c_int tile_overlap_percent = 10;

    memset(proximal_color, 0, 3 * sizeof(int));
    sprintf((char*)output_filename,"%s","map.json");

    for (i=1; i<argc; i+=2) {
        if ((strcmp(argv[i],"-f")==0) ||
                (strcmp(argv[i],"--filename")==0)) {
            filename = argv[i+1];
        }
        if ((strcmp(argv[i],"-o")==0) ||
                (strcmp(argv[i],"--output")==0)) {
            sprintf((char*)output_filename,"%s",argv[i+1]);
        }
        if ((strcmp(argv[i],"-pc")==0) ||
                (strcmp(argv[i],"--coverage")==0)) {
            proximal_coverage = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-pr")==0) ||
                (strcmp(argv[i],"--proximalred")==0)) {
            proximal_color[0] = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-pg")==0) ||
                (strcmp(argv[i],"--proximalgreen")==0)) {
            proximal_color[1] = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-pb")==0) ||
                (strcmp(argv[i],"--proximalblue")==0)) {
            proximal_color[2] = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--proxradius")==0) ||
                (strcmp(argv[i],"--proximalradius")==0)) {
            proximal_radius = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--roadcov")==0) ||
                (strcmp(argv[i],"--roadcoverage")==0)) {
            road_coverage = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--roadminorredthreshold")==0) ||
                (strcmp(argv[i],"--roadminorredthresh")==0)) {
            road_minor_threshold_red = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--roadminorgreenthreshold")==0) ||
                (strcmp(argv[i],"--roadminorgreenthresh")==0)) {
            road_minor_threshold_green = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--roadminorminred")==0) ||
                (strcmp(argv[i],"--roadminorminr")==0)) {
            road_minor_min_red = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--roadminormaxred")==0) ||
                (strcmp(argv[i],"--roadminormaxr")==0)) {
            road_minor_max_red = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--linesearch")==0) ||
                (strcmp(argv[i],"--linesrch")==0)) {
            line_search_radius = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--woodsthreshold")==0) ||
                (strcmp(argv[i],"--woodsthresh")==0)) {
            woods_threshold = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--woodscoverage")==0) ||
                (strcmp(argv[i],"--woodscov")==0)) {
            woods_coverage = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--watercoverage")==0) ||
                (strcmp(argv[i],"--watercov")==0)) {
            water_coverage = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--seacoverage")==0) ||
                (strcmp(argv[i],"--seacov")==0)) {
            sea_coverage = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--searedlow")==0) ||
                (strcmp(argv[i],"-srl")==0)) {
            sea_threshold_red_low = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--searedhigh")==0) ||
                (strcmp(argv[i],"-srh")==0)) {
            sea_threshold_red_high = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--seagreenlow")==0) ||
                (strcmp(argv[i],"-sgl")==0)) {
            sea_threshold_green_low = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--seagreenhigh")==0) ||
                (strcmp(argv[i],"-sgh")==0)) {
            sea_threshold_green_high = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--seabluelow")==0) ||
                (strcmp(argv[i],"-sbl")==0)) {
            sea_threshold_blue_low = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--seabluehigh")==0) ||
                (strcmp(argv[i],"-sbh")==0)) {
            sea_threshold_blue_high = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--maxsize")==0) ||
                (strcmp(argv[i],"-maxs")==0)) {
            max_building_size = atoi(argv[i+1]);
            max_building_polygon_width = 1700*max_building_size/100;
            max_building_polygon_height = 1000*max_building_size/100;
        }
        if ((strcmp(argv[i],"--minstationsize")==0) ||
                (strcmp(argv[i],"-minstatsize")==0)) {
            min_station_size = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--maxstationsize")==0) ||
                (strcmp(argv[i],"-maxstatsize")==0)) {
            max_station_size = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--seathreshold")==0) ||
                (strcmp(argv[i],"-st")==0)) {
            sea_threshold = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--roadlinkradius")==0) ||
                (strcmp(argv[i],"-rlr")==0)) {
            road_line_link_radius = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--riverlinkradius")==0) ||
                (strcmp(argv[i],"-rivlr")==0)) {
            water_line_link_radius = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"--overlap")==0) ||
                (strcmp(argv[i],"-olap")==0)) {
            tile_overlap_percent = atoi(argv[i+1]);
        }
    }

    /* was a file specified */
    if (filename == NULL) {
        printf("No image file specified\n");
        return 0;
    }

    image_data = read_png_file(filename, &image_width, &image_height, &image_bitsperpixel);
    if (image_data == NULL) {
        printf("Couldn't load image %s\n", filename);
        return 0;
    }
    if ((image_width == 0) || (image_height==0)) {
        printf("Couldn't load image size %dx%d\n", image_width, image_height);
        return 0;
    }
    if (image_bitsperpixel == 0) {
        printf("Couldn't load image depth\n");
        return 0;
    }

    printf("Image: %s\n", filename);
    printf("Resolution: %dx%d\n", image_width, image_height);
    printf("Depth: %d\n", image_bitsperpixel);

    if ((image_width > MAX_TILE_WIDTH) || (image_height > MAX_TILE_HEIGHT)) {
        if ((image_width < MIN_SOURCE_MAP_IMAGE_WIDTH) ||
                (image_height < MIN_SOURCE_MAP_IMAGE_HEIGHT)) {
            printf("Large images are expected to be at least %dx%d resolution.\n",
                   MIN_SOURCE_MAP_IMAGE_WIDTH, MIN_SOURCE_MAP_IMAGE_HEIGHT);
            free(image_data);
            return 1;
        }
        printf("A large map image was loaded. Creating tiles.\n");
        /* break up the map into tiles */
        if (breakup_large_map(filename,
                              image_data, image_width, image_height,
                              image_bitsperpixel, "tiles",
			      tile_overlap_percent) == 0) {
            printf("Tiles created\n");
        }
        free(image_data);
        printf("Ended Successfully\n");
        return 0;
    }

    original_data = (n_byte*)malloc(image_width*image_height*(image_bitsperpixel/8)*sizeof(n_byte));
    if (original_data == NULL) {
        printf("Unable to allocate original image data buffer\n");
        return 0;
    }

    /* make a copy of the original image data */
    memcpy(original_data, image_data, image_width*image_height*(image_bitsperpixel/8));

    if (image_bitsperpixel != 3*8) {
        printf("Expected 3 bytes per pixel\n");
        return 0;
    }

    thresholded = (n_byte*)malloc(image_width*image_height*sizeof(n_byte));
    if (thresholded == NULL) {
        printf("Unable to allocate memory for thresholded image");
    }

    thresholded_ref = (n_byte*)malloc(image_width*image_height*sizeof(n_byte));
    if (thresholded_ref == NULL) {
        printf("Unable to allocate memory for thresholded reference image");
    }

    possible_roads = (n_byte*)malloc(image_width*image_height*sizeof(n_byte));
    if (possible_roads == NULL) {
        printf("Unable to allocate memory for potential roads image");
    }

    /* detect sea/lakes */
    memcpy(image_data, original_data, image_width*image_height*(image_bitsperpixel/8));
    detect_color(image_data, image_width, image_height, image_bitsperpixel, 1,
                 sea_threshold_red_low, sea_threshold_red_high,
                 sea_threshold_green_low, sea_threshold_green_high,
                 sea_threshold_blue_low, sea_threshold_blue_high,
                 sea_averaging_radius);
    write_png_file("sea_stage1.png", image_width, image_height, 24, image_data);
    /* extract polygons for sea */
    color_to_binary(image_data, image_width, image_height,
                    image_bitsperpixel, 250, thresholded);
    proximal_erase(thresholded, image_width, image_height,
                   10, sea_coverage);
    no_of_sea = \
                proximal_fill(thresholded, image_width, image_height,
                              image_data,
                              min_sea_polygon_width, min_sea_polygon_height,
                              image_width, image_height,
                              4,
                              sea_id,
                              sea_vertices,
                              sea,
                              MAX_TOTAL_POLYGON_POINTS, 0);
    /* detection of sea or lakes just depends upon the percentage of the image
       covered by water color */
    sea_area_percent =
        get_polygons_total_area(image_width, image_height,
                                no_of_sea, sea_vertices, sea);
    printf("Water area: %d%%\n", sea_area_percent);
    if (sea_area_percent > sea_threshold) {
        sprintf(water_area_name,"%s","sea");
    }
    else {
        sprintf(water_area_name,"%s","lake");
    }
    sprintf(water_filename,"%s_stage2.png", water_area_name);
    write_png_file(water_filename,
                   image_width, image_height, 24, image_data);
    printf("%d %s areas\n", no_of_sea, water_area_name);
    /* save polygon image for sea */
    show_polygons(image_data, image_width, image_height,
                  no_of_sea,
                  sea_id,
                  sea_vertices,
                  sea);
    sprintf(water_filename,"%s.png", water_area_name);
    write_png_file(water_filename, image_width, image_height, 24, image_data);

    /* detect buildings */
    memcpy(image_data, original_data, image_width*image_height*(image_bitsperpixel/8));
    proximal_threshold(image_data, image_width, image_height,
                       proximal_color[0], proximal_color[1], proximal_color[2],
                       proximal_radius, thresholded);
    remove_nogo_areas(thresholded, image_width, image_height,
                      no_of_sea, sea_vertices, sea);
    potential_roads(thresholded, image_width, image_height,
		    possible_roads,
		    min_possible_road_width, max_possible_road_width);
    mono_to_color(possible_roads, image_width, image_height,
                  image_bitsperpixel, image_data);
    write_png_file("roads_possible.png", image_width, image_height, 24, image_data);
    mono_to_color(thresholded, image_width, image_height,
                  image_bitsperpixel, image_data);
    write_png_file("buildings_stage1.png", image_width, image_height, 24, image_data);
    current_coverage = percent_coverage(thresholded,image_width, image_height);
    printf("Building coverage: %d\n", current_coverage);
    /* adjust the coverage threshold so that it's lower in denser urban areas */
    adjusted_coverage = proximal_coverage - ((current_coverage - 16)*2);
    if (adjusted_coverage < 10) adjusted_coverage = 10;
    if (adjusted_coverage > 25) adjusted_coverage = 25;
    /* remove areas without enough coverage */
    proximal_erase(thresholded, image_width, image_height,
                   20, adjusted_coverage);
    /* make a copy of the binary image which can be used to check
       links later on */
    memcpy(thresholded_ref, thresholded, image_width*image_height*sizeof(n_byte));
    mono_to_color(thresholded, image_width, image_height,
                  image_bitsperpixel, image_data);
    write_png_file("buildings_stage2.png", image_width, image_height, 24, image_data);

    /* extract polygons for buildings */
    no_of_polygons = \
                     proximal_fill(thresholded_ref, image_width, image_height,
                                   image_data,
                                   0, 0,
                                   max_building_polygon_width,
                                   max_building_polygon_height,
                                   4,
                                   polygon_id,
                                   polygon_vertices,
                                   polygons,
                                   MAX_TOTAL_POLYGON_POINTS, 1);

    /* save building areas */
    write_png_file("buildings_stage3.png",
                   image_width, image_height, 24, image_data);

    printf("Polygons %d\n", no_of_polygons);

    /* save polygon fit image */
    show_polygons_against_reference(image_data, image_width, image_height,
                                    thresholded,
                                    no_of_polygons,
                                    polygon_id,
                                    polygon_vertices,
                                    polygons, 1);
    write_png_file("buildings_polygon_fit_inner.png", image_width, image_height, 24, image_data);
    show_polygons_against_reference(image_data, image_width, image_height,
                                    thresholded,
                                    no_of_polygons,
                                    polygon_id,
                                    polygon_vertices,
                                    polygons, 0);
    write_png_file("buildings_polygon_fit_outer.png", image_width, image_height, 24, image_data);
    /* save polygon image */
    show_polygons(image_data, image_width, image_height,
                  no_of_polygons,
                  polygon_id,
                  polygon_vertices,
                  polygons);

    /* save the image */
    write_png_file("buildings.png", image_width, image_height, 24, image_data);

    /* detect woodland */
    memcpy(image_data, original_data, image_width*image_height*(image_bitsperpixel/8));
    detect_green(image_data, image_width, image_height, image_bitsperpixel,
                 woods_threshold, woods_averaging_radius);
    write_png_file("woods_stage1.png", image_width, image_height, 24, image_data);
    /* extract polygons for woodland */
    color_to_binary(image_data, image_width, image_height,
                    image_bitsperpixel, 250, thresholded);
    proximal_erase(thresholded, image_width, image_height,
                   10, woods_coverage);
    no_of_woods = \
                  proximal_fill(thresholded, image_width, image_height,
                                image_data,
                                0, 0, 1700, 1000,
                                4,
                                woods_id,
                                woods_vertices,
                                woods,
                                MAX_TOTAL_POLYGON_POINTS, 0);
    write_png_file("woods_stage2.png",
                   image_width, image_height, 24, image_data);
    printf("%d woodland areas\n", no_of_woods);
    /* save polygon image */
    show_polygons(image_data, image_width, image_height,
                  no_of_woods,
                  woods_id,
                  woods_vertices,
                  woods);
    write_png_file("woods.png", image_width, image_height, 24, image_data);

    /* detect water */
    memcpy(image_data, original_data, image_width*image_height*(image_bitsperpixel/8));
    detect_blue(image_data, image_width, image_height, image_bitsperpixel,
                water_threshold_red,
                water_threshold_green,
                water_averaging_radius,
                water_min_blue,
                water_max_blue);
    write_png_file("water_stage1.png", image_width, image_height, 24, image_data);
    color_to_binary(image_data, image_width, image_height,
                    image_bitsperpixel, water_max_blue, thresholded);
    proximal_erase(thresholded, image_width, image_height,
                   10, water_coverage);
    /* make a copy of the binary image which can be used to check
       links later on */
    memcpy(thresholded_ref, thresholded, image_width*image_height*sizeof(n_byte));
    skeletonize(thresholded, image_width, image_height,
                image_data, image_bitsperpixel, max_river_width);
    write_png_file("water_stage2.png", image_width, image_height, 24, image_data);
    no_of_water_points =
        skeleton_to_points(image_data, image_width, image_height,
                           image_bitsperpixel, road_point_spacing,
                           water_points, MAX_ROAD_POINTS);
    no_of_water_points =
        remove_close_points(water_points, no_of_water_points,
                            road_point_spacing/2);
    printf("%d water line points\n", no_of_water_points);
    show_line_points(image_data, image_width, image_height,
                     water_points, no_of_water_points);
    write_png_file("water_stage3.png", image_width, image_height, 24, image_data);
    no_of_links =
        link_line_points(water_points, no_of_water_points,
                         water_line_link_radius, water_links,
                         thresholded_ref, image_width, image_height);
    line_join_ends(water_points, no_of_water_points,
                   water_links, river_join_ends_radius);
    printf("%d water point links\n", no_of_links);
    show_lines(image_data, image_width, image_height, image_bitsperpixel,
               water_points, no_of_water_points,
               water_links, 2, 255, 0, 0);
    write_png_file("water_stage4.png",
                   image_width, image_height, 24, image_data);

    /* detect main roads */
    memcpy(image_data, original_data, image_width*image_height*(image_bitsperpixel/8));
    detect_red(image_data, image_width, image_height, image_bitsperpixel,
               road_main_threshold_red,
               road_main_threshold_green,
               road_main_averaging_radius,
               road_main_min_red,
               road_main_max_red,
               road_main_max_green);
    write_png_file("roads_main_stage1.png", image_width, image_height, 24, image_data);
    color_to_binary(image_data, image_width, image_height,
                    image_bitsperpixel, 127, thresholded);
    /* make a copy of the binary image which can be used to check
       links later on */
    memcpy(thresholded_ref, thresholded, image_width*image_height*sizeof(n_byte));
    mono_to_color(thresholded, image_width, image_height,
                  image_bitsperpixel, image_data);
    write_png_file("roads_main_stage2.png",
                   image_width, image_height, 24, image_data);
    proximal_erase(thresholded, image_width, image_height,
                   10, road_coverage);
    skeletonize(thresholded, image_width, image_height,
                image_data, image_bitsperpixel, max_road_width);
    write_png_file("roads_main_stage3.png", image_width, image_height, 24, image_data);
    /* detect railway stations */
    no_of_stations =
        detect_blobs(thresholded_ref, image_width, image_height,
                     line_search_radius, MAX_STATION_POINTS, station_points,
                     min_station_size, max_station_size, image_data);
    if (no_of_stations > 0) {
        printf("%d station points\n", no_of_stations);
        show_line_points(image_data, image_width, image_height,
                         station_points, no_of_stations);
        write_png_file("stations.png", image_width, image_height, 24, image_data);
    }
    /* continue with main road detection */
    skeletonize(thresholded, image_width, image_height,
                image_data, image_bitsperpixel, max_road_width);
    no_of_main_road_points =
        skeleton_to_points(image_data, image_width, image_height,
                           image_bitsperpixel, road_point_spacing,
                           main_road_points, MAX_ROAD_POINTS);
    /* remove any road points which are railway stations */
    no_of_main_road_points =
        remove_blobs_from_points(station_points, no_of_stations,
                                 main_road_points, no_of_main_road_points,
                                 max_station_size);
    /* remove surplus main road points */
    no_of_main_road_points =
        remove_close_points(main_road_points, no_of_main_road_points,
                            road_point_spacing/2);
    show_line_points(image_data, image_width, image_height,
                     main_road_points, no_of_main_road_points);
    printf("%d main road line points\n", no_of_main_road_points);
    write_png_file("roads_main_stage4.png", image_width, image_height, 24, image_data);
    no_of_links =
        link_line_points(main_road_points, no_of_main_road_points,
                         road_line_link_radius, main_road_links,
                         thresholded_ref, image_width, image_height);
    printf("%d main road point links\n", no_of_links);
    show_lines(image_data, image_width, image_height, image_bitsperpixel,
               main_road_points, no_of_main_road_points,
               main_road_links, 4, 100, 100, 255);
    write_png_file("roads_main_stage5.png", image_width, image_height, 24, image_data);

    /* detect minor roads */
    memcpy(image_data, original_data, image_width*image_height*(image_bitsperpixel/8));
    detect_orange(image_data, image_width, image_height, image_bitsperpixel,
                  road_minor_threshold_red,
                  road_minor_threshold_green,
                  road_minor_averaging_radius,
                  road_minor_min_red,
                  road_minor_max_red);
    write_png_file("roads_minor_stage1.png",
                   image_width, image_height, 24, image_data);
    color_to_binary(image_data, image_width, image_height,
                    image_bitsperpixel, 252, thresholded);
    filter_potential_roads(thresholded, image_width, image_height,
			   possible_roads);
    /* make a copy of the binary image which can be used to check
       links later on */
    memcpy(thresholded_ref, thresholded, image_width*image_height*sizeof(n_byte));
    mono_to_color(thresholded, image_width, image_height,
                  image_bitsperpixel, image_data);
    write_png_file("roads_minor_stage2.png",
                   image_width, image_height, 24, image_data);
    proximal_erase(thresholded, image_width, image_height,
                   10, road_coverage);
    skeletonize(thresholded, image_width, image_height,
                image_data, image_bitsperpixel, max_road_width);
    write_png_file("roads_minor_stage3.png",
                   image_width, image_height, 24, image_data);
    no_of_minor_road_points =
        skeleton_to_points(image_data, image_width, image_height,
                           image_bitsperpixel, road_point_spacing,
                           minor_road_points, MAX_ROAD_POINTS);
    /* remove surplus minor road points */
    no_of_minor_road_points =
        remove_close_points(minor_road_points, no_of_minor_road_points,
                            road_point_spacing/2);
    printf("%d minor road line points\n", no_of_minor_road_points);
    write_png_file("roads_minor_stage4.png",
                   image_width, image_height, 24, image_data);
    show_line_points(image_data, image_width, image_height,
                     minor_road_points, no_of_minor_road_points);
    write_png_file("roads_minor_stage5.png",
                   image_width, image_height, 24, image_data);
    no_of_links =
        link_line_points(minor_road_points, no_of_minor_road_points,
                         road_line_link_radius, minor_road_links,
                         thresholded_ref, image_width, image_height);
    printf("%d minor road point links\n", no_of_links);
    show_lines(image_data, image_width, image_height, image_bitsperpixel,
               minor_road_points, no_of_minor_road_points,
               minor_road_links, 4, 100, 150, 255);
    write_png_file("roads_minor_stage6.png",
                   image_width, image_height, 24, image_data);

    /* bridges */
    no_of_bridges =
        detect_crossings(main_road_points, no_of_main_road_points,
                         main_road_links,
                         minor_road_points, no_of_minor_road_points,
                         minor_road_links,
                         water_points, no_of_water_points,
                         water_links,
                         bridge_points, MAX_BRIDGES);
    printf("%d bridges\n", no_of_bridges);

    /* junctions */
    no_of_junctions =
        detect_junctions(main_road_points, no_of_main_road_points,
                         main_road_links,
                         minor_road_points, no_of_minor_road_points,
                         minor_road_links,
                         junction_points, MAX_JUNCTIONS,
                         minor_road_join_ends_radius);
    printf("%d junctions\n", no_of_junctions);
    /* all roads */
    show_three_lines(image_data, image_width, image_height,
                     image_bitsperpixel,
                     water_points, no_of_water_points,
                     water_links, 5, 255, 200, 200,
                     minor_road_points, no_of_minor_road_points,
                     minor_road_links, 4, 100, 150, 255,
                     main_road_points, no_of_main_road_points,
                     main_road_links, 8, 100, 100, 255,
                     junction_points, no_of_junctions);
    show_polygons_filled(image_data, image_width, image_height,
                         no_of_polygons,
                         polygon_vertices,
                         polygons,
                         200, 200, 200);
    show_polygons_empty(image_data, image_width, image_height,
                        no_of_polygons,
                        polygon_vertices,
                        polygon_id,
                        polygons);
    show_polygons_filled(image_data, image_width, image_height,
                         no_of_woods,
                         woods_vertices,
                         woods,
                         200, 255, 200);
    show_crossings(image_data, image_width, image_height, image_bitsperpixel,
                   bridge_points, no_of_bridges,
                   50, 50, 50, 0, 0, 255,
                   20, 10);
    show_polygons_filled(image_data, image_width, image_height,
                         no_of_sea,
                         sea_vertices,
                         sea,
                         255, 100, 100);
    write_png_file("roads.png",
                   image_width, image_height, 24, image_data);

    /* save a json file for this map */
    save_map_json(filename, no_of_polygons,
                  polygon_id,
                  polygon_vertices,
                  polygons,
                  no_of_main_road_points,
                  main_road_points,
                  main_road_links,
                  no_of_minor_road_points,
                  minor_road_points,
                  minor_road_links,
                  no_of_woods,
                  woods_id,
                  woods_vertices,
                  woods,
                  no_of_sea,
                  sea_id,
                  sea_vertices,
                  sea,
                  no_of_water_points,
                  water_points,
                  water_links,
                  no_of_stations,
                  station_points,
                  sea_area_percent, sea_threshold,
                  bridge_points, no_of_bridges,
                  junction_points, no_of_junctions,
                  image_width, image_height,
                  output_filename);

    /* free memory */
    free(possible_roads);
    free(thresholded);
    free(thresholded_ref);
    free(image_data);
    free(original_data);

    printf("Ended Successfully\n");
    return 0;
}
