//
//  main.c
//  london
//
//  Created by Thomas Barbalet on 10/8/20.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <zlib.h>
#include "pnglite.h"

//extern unsigned char * read_png_file(char * filename, png_t * ptr);
//extern int write_png_file(char* filename, int width, int height, unsigned char *buffer);


static unsigned long color_count[32*32*32] = {0};

#define TOTAL_COLORS 128

static int used_colors[TOTAL_COLORS] =
{
    31679, 24446, 24409, 24312, 24307, 23452, 23382, 23359, 23355, 23254, 23218, 22462, 22365, 22328, 22293, 22266, 21375, 21371, 21278, 21239, 21235, 21173, 21168, 20284, 20249, 20152, 20115, 20079, 20013, 19946, 19263, 19197, 19189, 19154, 19094, 19028, 19025, 18888, 18168, 18143, 18100, 18064, 17998, 17110, 17013, 17010, 16944, 16909, 15924, 15857, 15822, 15071, 14996, 14929, 14894, 14828, 13842, 13807, 13741, 13738, 13177, 12959, 12753, 12652, 12119, 11792, 11757, 11695, 11657, 11160, 11030, 10847, 10668, 10606, 10571, 10568, 10102, 10073, 9972, 9482, 9479, 9015, 8914, 8735, 8589, 8554, 8492, 8021, 7955, 7496, 7401, 7398, 6897, 6654, 6442, 6340, 6312, 5695, 5567, 5564, 5494, 5384, 5254, 5251, 4506, 4436, 4368, 4333, 4330, 4326, 3583, 3486, 3448, 3272, 3237, 3223, 3220, 3175, 3170, 2428, 2251, 2162, 2159, 2117, 1158, 1142, 1059, 84
};

void solve_color(void)
{
    int loopy = 0;
//    while (loopy < (TOTAL_COLORS-1))
//    {
//        int loopx = loopy + 1;
//        while (loopx < TOTAL_COLORS)
//        {
//            if (loopx != loopy)
//            {
//                int value = used_colors[loopy];
//                if (value != -1)
//                {
//                    int ideal_red =    value & 31;  // 7
//                    int ideal_green = (value >> 5) & 31; // 7
//                    int ideal_blue =  (value >> 10) & 31; // 7
//                    int value2 = used_colors[loopx];
//                    if (value2 != -1)
//                    {
//                        int red =    value2 & 31;  // 7
//                        int green = (value2 >> 5) & 31; // 7
//                        int blue =  (value2 >> 10) & 31; // 7
//                        int delta_red = ideal_red - red;
//                        int delta_green = ideal_green - green;
//                        int delta_blue = ideal_blue - blue;
//                        unsigned long total = (delta_red * delta_red) + (delta_green * delta_green) + (delta_blue * delta_blue);
//                        if (total < 10)
//                        {
//                            used_colors[loopx] = -1;
//                        }
//                    }
//                }
//            }
//            loopx++;
//        }
//        loopy++;
//    }
    loopy = 0;
    while (loopy < (TOTAL_COLORS))
    {
        if (used_colors[loopy] != -1)
        {
            int value = used_colors[loopy];
            int ideal_red =    value & 31;  // 7
            int ideal_green = (value >> 5) & 31; // 7
            int ideal_blue =  (value >> 10) & 31; // 7
            printf("%d, %d, %d\n", ideal_red * 8, ideal_green * 8, ideal_blue * 8);
        }
        loopy++;
    }
}


unsigned char index_rgb(unsigned char * values)
{
    unsigned long max_difference = 0xffffffffffffffff;
    unsigned char max_index = 255;
    int red =   values[0] >> 3;  // 7
    int green = values[1] >> 3; // 7
    int blue =  values[2] >> 3; // 7
    int loop = 0;
    
    while (loop < TOTAL_COLORS)
    {
        unsigned int ideal = used_colors[loop];
        int ideal_red =    ideal & 31;  // 7
        int ideal_green = (ideal >> 5) & 31; // 7
        int ideal_blue =  (ideal >> 10) & 31; // 7
        int delta_red = ideal_red - red;
        int delta_green = ideal_green - green;
        int delta_blue = ideal_blue - blue;
        unsigned long total = (delta_red * delta_red) + (delta_green * delta_green) + (delta_blue * delta_blue);
        if (total == 0)
        {
            values[0] = ideal_red * 8;
            values[1] = ideal_green * 8;
            values[2] = ideal_blue * 8;
            return loop;
        }
        if (max_difference > total)
        {
            max_difference = total;
            max_index = loop;
            values[0] = ideal_red * 8;
            values[1] = ideal_green * 8;
            values[2] = ideal_blue * 8;
        }
        loop++;
    }
    if (max_index == 255)
    {
        printf("ERROR coverage\n");
    }
    return max_index;
}



void process_rgb(unsigned char * values)
{
    int red = values[0] >> 3;  // 7
    int green = values[1] >> 3; // 7
    int blue = values[2] >> 3; // 7
    int count = red + (green * 32) + (blue * 32 * 32);
    
    color_count[count] ++;
}

void process_count(char * string)
{
    int loop = 0;
    int natural = 0;
    while (loop < (32*32*32))
    {
        unsigned long count = color_count[loop];
        if (count > 10000)
        {
            printf("%d, %ld, \"%s\",\n", loop, count, string);
            natural++;
        }
        loop++;
    }
    printf("\\ \"%s\" %d\n",string, natural);
}

void create_indexfile(unsigned char * png_buffer, char * filename)
{
    unsigned char * index_file = malloc(11100 * 13500);

    unsigned long loop = 0;
    while (loop < (11100 * 13500))
    {
        index_file[loop] = index_rgb(&png_buffer[3*loop]);
        loop++;
    }
    {
        FILE * output_file = fopen(filename, "wb");
        fwrite(index_file, 11100 * 13500, 1, output_file);
        fclose(output_file);
    }
    
    free(index_file);
}

void make_swatch(void)
{
    // create colormap swatch
//    unsigned char color_map[256 * 256 * 3] = {0};
//    int loopy = 0;
//
//    while (loopy < 128)
//    {
//        int loopx = 0;
//        while (loopx < 256)
//        {
//            int locx = loopx / 16;
//            int locy = loopy / 16;
//            int point = locx + (locy * 16);
//
//            if (point < TOTAL_COLORS)
//            {
//                unsigned int ideal = used_colors[point];
//                int ideal_red =    ideal & 31;  // 7
//                int ideal_green = (ideal >> 5) & 31; // 7
//                int ideal_blue =  (ideal >> 10) & 31; // 7
//                int color_location = ((loopy * 256) + loopx)*3;
//                color_map[color_location] = ideal_red * 8;
//                color_map[color_location+1] = ideal_green * 8;
//                color_map[color_location+2] = ideal_blue * 8;
//            }
//            loopx++;
//        }
//        loopy++;
//    }
//    (void)write_png_file("swatch.png", 256, 128, color_map);
}

int main(int argc, const char * argv[])
{
    png_t temp;
    
    if (argc == 1)
    {
        //make_swatch();
        solve_color();
    }
    
//    if (argc == 4)
//    {
//        unsigned char * png_buffer = read_png_file((char *)argv[1], &temp);
//        if (png_buffer)
//        {
//            create_indexfile(png_buffer, (char *)argv[3]);
//        }
//        printf("flatten rgb\n");
//        int ret_val = write_png_file((char *)argv[2], 11100, 13500, png_buffer);
//
//        free(png_buffer);
//        printf("run succeeded %d\n", ret_val);
//    }
    
//    if (argc == 2)
//    {
//        unsigned char * png_buffer = read_png_file((char *)argv[1], &temp);
//        if (png_buffer)
//        {
//            int ret_val = 0;
//
//            unsigned long loop = 0;
//            while (loop < (11100 * 13500))
//            {
//                process_rgb(&png_buffer[3*loop]);
//                loop++;
//            }
//
//            process_count((char *)argv[1]);
//
//
////            printf("flatten rgb\n");
////            ret_val = write_png_file((char *)argv[2], 11100, 13500, png_buffer);
//
//            free(png_buffer);
//            printf("run succeeded %d\n", ret_val);
//        }
//        else
//        {
//            printf("run failed\n");
//        }
//    }
    return 1;
}
