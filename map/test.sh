#!/bin/bash
rm -f result.png .#* src/.#* thresholded.png road*.png sea*.png water*.png woods*.png buildings*.png lake*.png streets*.png
rm -f map.json
./map2json -f folkestone/folkestone.png
# ./map2json -f ashford/ashford.png
# ./map2json -f images/5_2-51727_431-51686_328-3870_6025.png --coverage 16
# ./map2json -f images/9_0-51576_618-51535_530-564_10076.png
